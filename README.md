# PWA E-Commerce Test App

# Live Demo https://frontend-react-ecommerce.netlify.app

# Steps to `run` this project

### 1. First Install packages using command `npm install`

### 2. After installing node_modules simply run command `npm start`

# Steps to `Build` and `Deploy` project on Netlify

### 1. Run command `npm run build`

### 2. Netlify login first using command `netlify login`

### 3. after that run command `npm run deploy`
