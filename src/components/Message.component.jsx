import React from "react";
import { Alert } from "react-bootstrap";

const Message = ({ variant, children }) => {
  //using Alert component from react-bootstrap to show message on the basis of error or success
  return <Alert variant={variant}>{children}</Alert>;
};

//defined default value of prop
Message.defaultProps = {
  variant: "info",
};

export default Message;
