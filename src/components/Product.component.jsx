import React from "react";
import { Link } from "react-router-dom";
import { Card, Button } from "react-bootstrap";
import { Rating } from ".";

const Product = ({ product, history }) => {
  //this function is navigating to cart page and passing product id to add that item in cart using redux
  const addToCartHandler = () => {
    history.push(`/cart/${product.id}`);
  };
  return (
    <Card className="my-3 p-3 rounded">
      <Link to={`/product/${product.id}`} aria-label="product-image">
        <Card.Img src={product.image} variant="top" alt={product.title} />
      </Link>
      <Card.Body className="card-body">
        <div>
          <Link to={`/product/${product.id}`}>
            <Card.Title as="div">
              <strong>{product.title}</strong>
            </Card.Title>
          </Link>
          <Card.Text as="div">
            {/* Rating is custom component to show number of reviews and total stars of each product */}
            <Rating
              value={product.rating}
              text={`${product.numReviews} reviews`}
              color="red"
            />
          </Card.Text>

          <Card.Text as="h3">${product.price}</Card.Text>
        </div>
      </Card.Body>
      <Card.Footer>
        {/* quick add button for adding item directly from home page */}
        <Button
          onClick={addToCartHandler}
          className="btn btn-block"
          type="button"
          //disable button if product is not in a stock
          disabled={product.countInStock === 0}
        >
          Quick Add
        </Button>
      </Card.Footer>
    </Card>
  );
};

export default Product;
