import React from "react";
import { Spinner } from "react-bootstrap";

const Loader = () => {
  //using Loader while performing async operation
  //spinner component is from react-bootstrap
  return (
    <Spinner
      animation="border"
      role="status"
      style={{
        width: "100px",
        height: "100px",
        margin: "auto",
        display: "block",
      }}
    >
      <span className="sr-only">Loading...</span>
    </Spinner>
  );
};

export default Loader;
