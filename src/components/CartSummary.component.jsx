import React from "react";
import { ListGroup, Card, Button } from "react-bootstrap";

const CartSummary = ({ cartItems, handleCheckout }) => {
  return (
    <Card>
      <Card.Header align="center" className="align-center">
        <h2>Cart Summary</h2>
      </Card.Header>
      <ListGroup variant="flush">
        <ListGroup.Item>
          <h3>
            Subtotal ({cartItems.reduce((acc, item) => acc + item.qty, 0)}){" "}
            items
          </h3>
          $
          {cartItems
            .reduce((acc, item) => acc + item.qty * item.price, 0)
            .toFixed(2)}
        </ListGroup.Item>
        {cartItems.length ? (
          <>
            <ListGroup.Item>
              <h3>Tax per product</h3>
              1.23%
            </ListGroup.Item>
            <ListGroup.Item>
              <h3>Shipping Total</h3>$5
            </ListGroup.Item>
            <ListGroup.Item>
              <h3>Grand Total</h3>$
              {cartItems
                .reduce(
                  (acc, item) =>
                    acc +
                    item.qty * item.price +
                    (item.price * (1.23 / 100) + 5),
                  0
                )
                .toFixed(2)}
            </ListGroup.Item>
          </>
        ) : (
          ""
        )}
        <ListGroup.Item>
          <Button
            type="button"
            className="btn btn-block"
            disabled={cartItems.length === 0}
            onClick={handleCheckout}
          >
            Proceed to Checkout
          </Button>
        </ListGroup.Item>
      </ListGroup>
    </Card>
  );
};

export default CartSummary;
