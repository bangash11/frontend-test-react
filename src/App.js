import React from "react";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Header, Footer } from "./components";
import {
  HomeScreen,
  ProductDetailScreen,
  CartScreen,
  OrderScreen,
} from "./screens";

const App = () => {
  // Main routing defined here for all screens
  return (
    <Router>
      <Header />
      <main>
        <Container>
          <Switch>
            <Route exact path="/" component={HomeScreen} />
            <Route exact path="/login" component={() => <div>Login</div>} />
            <Route exact path="/cart/:id?" component={CartScreen} />
            <Route exact path="/check-out" component={OrderScreen} />
            <Route exact path="/product/:id" component={ProductDetailScreen} />
          </Switch>
        </Container>
      </main>
      <Footer />
    </Router>
  );
};

export default App;
