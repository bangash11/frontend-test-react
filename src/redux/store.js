import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
//redux devtools extension for redux debugging
import { composeWithDevTools } from "redux-devtools-extension";

// importing different reducers from reducers folder
import {
  productListReducer,
  productDetailReducer,
} from "./reducers/productReducers";
import { cartReducer } from "./reducers/cartReducers";

const reducer = combineReducers({
  productList: productListReducer,
  productDetails: productDetailReducer,
  cart: cartReducer,
});

// getting carts Item from localStorage
const cartItemFromStorage = localStorage.getItem("cartItems")
  ? JSON.parse(localStorage.getItem("cartItems"))
  : [];

// storing cart item from localStorage in initial state
const initialState = {
  cart: {
    cartItems: cartItemFromStorage,
  },
};
// thunk middleware for async redux-thunk operation
const middleware = [thunk];

// creating store by passing initialState, reducer and middleware
// composeWithDevTools for redux debugging for viewing state etc
const store = createStore(
  reducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
);
export default store;
