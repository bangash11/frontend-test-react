import React from "react";
import { Message } from "../components";
import { Container } from "react-bootstrap";
const OrderScreen = () => {
  return (
    <Container className="mt-5">
      <Message variant="success">{"Order Checkout Successfully"}</Message>
    </Container>
  );
};

export default OrderScreen;
