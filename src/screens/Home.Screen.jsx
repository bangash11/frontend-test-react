import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
//using components from react-bootstrap
import { Col, Row } from "react-bootstrap";
import { Product, Loader, Message } from "../components";
import { listProducts } from "../redux/actions/productActions";
const HomeScreen = ({ history }) => {
  // using redux hooks instead of connect HOC
  // useDispatch is for dispatching action
  const dispatch = useDispatch();
  // useSelector is for getting values from state
  const productList = useSelector((state) => state.productList);
  const { loading, error, products } = productList;
  useEffect(() => {
    // dispatching action for getting lists of all products
    dispatch(listProducts());
  }, [dispatch]);
  return (
    <>
      <h1>Latest products</h1>
      {loading ? (
        <Loader />
      ) : error ? (
        <Message variant="danger">{error}</Message>
      ) : (
        <Row>
          {/* Mapping all products from state to show in react-bootstrap columns component */}
          {products &&
            products.map((product) => (
              <Col
                key={product.id}
                sm={12}
                md={6}
                lg={4}
                xl={3}
                className="col-md-4"
              >
                {/* Product custom component */}
                <Product product={product} history={history} />
              </Col>
            ))}
        </Row>
      )}
    </>
  );
};

export default HomeScreen;
